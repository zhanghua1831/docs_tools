get_ci_info(){
curl -s -f -X GET --header 'Content-Type: application/json;charset=UTF-8' "https://gitee.com/api/v5/repos/src-openeuler/$name/pulls/$prnum/comments?access_token=$token&page=1&per_page=20" -o .file 
}

get_pr_status(){
curl -s -f -X GET --header 'Content-Type: application/json;charset=UTF-8' "https://gitee.com/api/v5/repos/src-openeuler/$name/pulls/$prnum?access_token=$token" -o .file1
}

create_ci_info_deal(){
cat > a.py <<XXX
import json;
ci_info_file = open(".file", mode="r")
pr_status_file = open(".file1", mode="r")
ci_data = json.load(ci_info_file)
status_data = json.load(pr_status_file)
#get pr state
print(status_data["state"],end=" ")

#get ci result
for i in range(len(ci_data)-1,-1,-1):
    try:
        ci_info = ci_data[i]["body"]
        if "task-check-code-style" in ci_info:
            break
        else:
            ci_info=""
    except IndexError:
        continue
if len(ci_info) == 0:
    print("UNBUILD")
    exit()

info_split = ci_info.split("|")
ci_info_1 = info_split[10].split("*")[2]
ci_info_2 = info_split[14].split("*")[2]
ci_info_3 = info_split[18].split("*")[2]
if ci_info_1 == "SUCCESS" and ci_info_2 == "SUCCESS" and ci_info_3 == "SUCCESS":
    print("SUCCESS")
else:
    print("FAILURE")
XXX
}

main(){
OUT='csvs'
if [[ -d ${OUT} ]];then
	mkdir -p $OUT
fi
b=${OUT}/$(date +'%Y%m%d%H%M%S')_$(hostname).csv

create_ci_info_deal
while read x;do
	cut=$(echo $x | awk '{print NF}')
	if [[ $cut -ge 4 ]];then
	    name=$(echo $x |awk '{print $1}')
	    prnum=$(echo $x |awk '{print $2}' |awk -F '/' '{print $7}')
	    a=$(echo "$x" |awk '{$3="";print $0;}'|sed 's/  / /g' )
	    echo -n "$a "
	    get_ci_info 
            get_pr_status
	    python3 a.py
	else 
            echo "$x"
	fi
done < $file | tee ${b}
rm -f a.py

sed -i 's/ /,/g' ${b}
sed -i '1i\PKG,LINK,USER,ASS,STATE,CI' ${b}

# do tail
pipe='home:songshuaishuai/for_trans'
if [[ -d $pipe && -f push_to_obs.sh ]];then
	a=$(find ${OUT} -type f -printf "%T@ %p\n" | sort -n | grep -v "$b" | tail -n1 | awk '{print $2}')
	if [[ -z ${a} ]];then
		bash push_to_obs.sh ${b} && echo "Hint: $b pushd to obs"
	else
		diff ${a} ${b} &>/dev/null || { bash push_to_obs.sh ${b} && echo "Hint: diff $a $b pushd to obs"; }
	fi
fi

}

file=$1
token=$2
main
