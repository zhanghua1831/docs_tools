## openeuler 工作
- [openeuler 工作](#openeuler-%e5%b7%a5%e4%bd%9c)
  - [工作环境](#%e5%b7%a5%e4%bd%9c%e7%8e%af%e5%a2%83)
  - [spec整改流程](#spec%e6%95%b4%e6%94%b9%e6%b5%81%e7%a8%8b)
  - [技术点](#%e6%8a%80%e6%9c%af%e7%82%b9)
  - [敏感信息](#%e6%95%8f%e6%84%9f%e4%bf%a1%e6%81%af)

### 工作环境 
1. vm                             # 工作环境
2. obs + jenkins                  # 自动化编译环境
3. gerrit                         # 只做内网检视  
4. gitee                          # 存储代码
5. qqdoc/dbox 软件包开发计划.xsl   # 同步工作进展



### spec整改流程
1. 拿到 src.rpm 
    - yumdownloader || wget  
2. rpmbuild1 
    - osc build  || 容器内 rpmbuild   
3. spec 整改
    - 整改规范 # 参考 [1] # 可参考别人改过的
    - 敏感信息 # tar包一致性 # patch 溯源 [参考](#%e6%95%8f%e6%84%9f%e4%bf%a1%e6%81%af)
    - 参考 [suse] [fedora] 
4. rpmbuild2 
5. rpm check
    - list provides install/update/erase function
    - 容器内执行 
6. code review
    - 内网 gerrit 检视
7. PR && merge 


### 技术点
- spec 语法
    - google rpm max 
- rpm  
    - i U e 
- rpm[tab]
    - rpm2cpio
    - rpmspec 
    - rpmdev* 
- rpmbuild 
    - 分阶段编译 --short-circuit 
- dnf 
    - download 
    - repoquery
    - builddep 
- osc 
- docker 
    - 参考 [2]
    - oe 非官方镜像 songshuaidocker/reluenepo:v2 
- git  
- repo 
    - gerrit交互工具，3ms搜索其用法
> rpm rpmbuild dnf osc # 建议把每个子命令都测试一遍  

### 敏感信息
1. tar包是否一致
2. patch 是否有 bz 对应 || patch 是否可在上游社区找到 || 备注patch功能 && 删除后的影响分析 
    - bz 去原始spec文件中查找 || google  
3. 配置文件倾向于删除 || 保留要说明原因
4. susu 对于以上文件的处理意见
```
example  byteman  remove_submit_integration_test_verification.patch

1. patch没有bugzilla对应， 
2. 上游社区无该补丁, 
3. patch功能： 删除部分集成测试
4. 删掉patch是否可行： 删除patch，编译会失败，意味着上游社区源码直接编译会有问题，需要基于编译报错定位
5. suse 无该包，不可参考 
```
> oe的主要工作内容其实并不是基于fc的包进行整改，而是基于当前版本的最上游社区代码进行整改，并以fc.src为参考
> 针对spec整改-敏感信息的开发原则之一：以上游社区和bz社区为准，最上游社区没有的东西能删就删掉







[1]:(https://gitee.com/sugarfillet/docs_tools/blob/master/spec_revise/spec_revise_detail.md)
[2]:(https://gitee.com/sugarfillet/docs_tools/blob/master/docker_build/docker_rpmbuild.md)
[suse]:(https://build.opensuse.org/project/show/openSUSE:Factory)
[fedora]:(https://src.fedoraproject.org/)
