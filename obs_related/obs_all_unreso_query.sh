#!/bin/bash
upload(){
SRC='src/'
file=$1
proj=$2
repo_n='shuai.repo'

if [[ ! -d $SRC ]];then
	mkdir $SRC
fi

old=$(osc ls $proj | wc -l)

#init proj
if [[ ! -d $proj ]];then
	osc co $proj &>/dev/null || exit 2
else
	pushd $proj
	osc up -S &>/dev/null || exit 2
	popd
fi

# config fedora & fedora-update source 
pushd /etc/yum.repos.d/

cat > $repo_n << EOF
[fedora-u-src]
name=fedora-u-src
baseurl=http://mirrors.aliyun.com/fedora/updates/29/Everything/SRPMS
enabled=1
gpgcheck=0
priority=5
[fedora-src]
name=fedora-src
baseurl=http://mirrors.aliyun.com/fedora/releases/29/Everything/source/tree
enabled=1
gpgcheck=0
priority=4
EOF


popd

for x in `cat $file`;do
# download src.rpm from fc
yumdownloader --source --repo fedora-src --repo fedora-u-src $x && mv $x-*.src.rpm $SRC \
&& osc importsrcpkg -p $proj -c -n $x $SRC/$x-*.src.rpm 
done

rm -f /etc/yum.repos.d/$repo_n
mv src $bbb
new=$(osc ls $proj | wc -l)
echo "======================"
echo "$proj pkgnum old $old new $new"
echo "======================"
}

cang='openEuler:function'
repo='standard_aarch64'
arch='aarch64'
repo_n='abcded.repo'


# get all unresolved pkg
echo "XXXXXX Get all un pkg"
osc pr $cang -s U -r $repo -a $arch -q -V | sed 1d \
| sed '$d' | sed '$d'| awk '{print $2}' >  unresolved_pkgs


if [[ ! -d msgs ]];then
	mkdir msgs
fi
# get pkgs' error msg

echo "XXXXXX Get all un pkgs'msg"
for x in `cat unresolved_pkgs`;do
	osc r $cang $x -r $repo -a $arch -v |grep -v unresolv \
| awk '{print $3}' >  msgs/$x.msg
done 

# config fedora & fedora-update source 
echo "XXXXXX Config fc bin repo"
pushd /etc/yum.repos.d/
cat > $repo_n <<eof
[fedora-u]
name=fedora-u 
baseurl=http://mirrors.aliyun.com/fedora/updates/29/Everything/aarch64
enabled=1
gpgcheck=0
priority=5

[fedora]
name=fedora
baseurl=http://mirrors.aliyun.com/fedora/releases/29/Everything/aarch64/os
enabled=1
gpgcheck=0
priority=4
eof
popd

# do query 
echo "XXXXXX Query bin and src"
for x in `cat unresolved_pkgs`;do
	for y in `cat msgs/$x.msg`;do
	bin=`dnf repoquery --whatprovides "$y" --repo fedora --repo fedora-u -q |head -n1 | sed 's/-[[:digit:]]\+.*//'`
	src=`dnf repoquery -q -i $bin --repo fedora --repo fedora-u |grep Source| head -n1 | awk '{print $3}'| sed 's/-[[:digit:]]\+.*//'`
	printf "%s\t%s\t%s\t%s\n" $x $y $bin $src
	done
done | tee result.txt

echo "XXXXXX PLEASE check the result.txt"

rm -rf /etc/yum.repos.d/$repo_n

bbb=`date +'%Y%m%d%H%M'`
mkdir $bbb
mv msgs unresolved_pkgs $bbb 
cp result.txt $bbb

#diff mainline 

echo "XXXXXX Refresh src list"
rm *.list -f

osc ls openEuler:Mainline > mainline.list
osc ls openEuler:function > function.list 
osc ls openEuler:function:rely > function_rely.list
cat *.list | sort -u > all.list
cat result.txt | awk '{print $4}' | sort -u |grep -v '^$'> to.list
cat all.list to.list | sort | uniq -d > dd.list
cat to.list dd.list | sort | uniq -u > final.list

ls *.list |grep -v final.list | xargs -I {} mv {} $bbb

# upload  to obs
echo "XXXXXX Do upload"
upload final.list openEuler:function:rely
mv final.list result.txt $bbb
